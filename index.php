<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>CM P&L Sheet</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<!--  <link rel="apple-touch-icon" href="apple-touch-icon.png">-->
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
    <script src="js/jquery.min.js"></script>
</head>

<body>
  <script>
        function get_active_financial_year(){
                var today = new Date();
                var cur_month = today.getMonth();
                var cur_year = today.getFullYear();

                if (cur_month > 2)
                  fin_year = cur_year;
                else
                  fin_year = cur_year - 1;
                   
                   fin_date = "31";
                   fin_month = "03";
                   fin_date = new Date(fin_year+"/"+fin_month+"/"+fin_date);
                   return fin_date;

            }
        jQuery(document).ready(function(){
            var  fin_date = get_active_financial_year();
            var year_2_date = new Date(fin_date);
            year_2_date = new Date(year_2_date.setFullYear(year_2_date.getFullYear() - 1));
            var year_3_date = new Date(fin_date);
            year_3_date = new Date(year_3_date.setFullYear(year_3_date.getFullYear() - 2));
            jQuery("#form_2 .bs_pl_sheet_date").html(fin_date.toLocaleDateString());
            jQuery("#form_1 .bs_pl_sheet_date").html(year_2_date.toLocaleDateString());
            jQuery("#form_0 .bs_pl_sheet_date").html(year_3_date.toLocaleDateString());
        
            <?php

            // $json_d = file_get_contents("payload.json");
            // if(isset($json_d)) 
            // {
            //     $post_json = json_decode($json_d);
            // }
         
            foreach ($_POST as $f_key=>$value)
            {
                if (is_array($value))
                {
                    foreach ($value as $index=>$val)
                    {
                        $el = "#form_".$index;
                        echo "jQuery(\"".$el." :input[name='".$f_key."[]']\").val(".$val.");\n";    
                    }
                }
            }
            ?>
            
            jQuery(':input[type="number"]').each(function(){
                if (jQuery(this).val() == '' )
                    {
                        this.value = 0;
                    }
                jQuery(this).attr('step','any');
                jQuery(this).val(Number(jQuery(this).val()).toFixed(2));
            })
            
            jQuery(':input[type="number"]').change(function(){
                //var target_el = '.form_container_0';
                var target_id = jQuery(this).closest('.form_container').first().attr("id");
                var target_el = "#" + target_id
                calc_bal_sheet(target_el);
                analysis(target_el);
                calc_turnover_ratios(target_el);
                calc_profitability_liquidity_ratios(target_el);
                gearing_solvency_ratios(target_el);
                calc_z_score(target_el);
                cash_flow_statement_from_operating_activites(target_el);
                operating_profit_before_working_capital(target_el);

                jQuery(':input[type="number"]').each(function(){
                    jQuery(this).val(Number(jQuery(this).val()).toFixed(2));
                });
            })
        });
function calc_bal_sheet(target_el){ 
                
                // Assets
                var total_ca = Number(jQuery(target_el+" :input[name='inventory[]']").val()) + Number(jQuery(target_el+" :input[name='receivable[]']").val()) + Number(jQuery(target_el+" :input[name='cash[]']").val()) + Number(jQuery(target_el+" :input[name='other_ca[]']").val());

               // cm_pl_total_ca = total_ca;
                
                jQuery(target_el+" :input[name='total_ca[]']").val(total_ca);
                
                var total_nca = Number(jQuery(target_el+" :input[name='fixed_assets[]']").val()) + Number(jQuery(target_el+" :input[name='investments[]']").val()) + Number(jQuery(target_el+" :input[name='other_assets[]']").val()) + Number(jQuery(target_el+" :input[name='intangibles[]']").val());
                
                jQuery(target_el+" :input[name='total_nca[]']").val(total_nca);
                
                jQuery(target_el+" :input[name='total_assets[]']").val(total_ca + total_nca);
                
                //Assets End
                
                //Liabilities Begin (part 1) 
                
                var total_cli = Number(jQuery(target_el+" :input[name='st_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='st_loan_others[]']").val()) + Number(jQuery(target_el+" :input[name='creditors[]']").val()) + Number(jQuery(target_el+" :input[name='ct_others[]']").val());
                
                jQuery(target_el+" :input[name='total_cl[]']").val(total_cli);

               // cm_pl_total_cl = total_cli;
                
                var total_nli = Number(jQuery(target_el+" :input[name='lt_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='lt_loan_others[]']").val()) + Number(jQuery(target_el+" :input[name='nc_others[]']").val());

                jQuery(target_el+" :input[name='total_ncl[]']").val(total_nli);
                
                //cm_pl_total_nli = total_nli;
                //Liabilities end (part 1) 
                
                //Equities begin
                var total_eq = Number(jQuery(target_el+" :input[name='capital[]']").val()) + Number(jQuery(target_el+" :input[name='reserve_surplus[]']").val());
                
                jQuery(target_el+" :input[name='total_equity[]']").val(total_eq);

                //cm_pl_equity = total_eq;
                //Equities end

                //Liabilities begin (part 2)
                jQuery(target_el+" :input[name='total_liabilities[]']").val(total_cli + total_nli + total_eq);
                //Liabilities end (part 2)
                
                //Balance Sheet End
                
                //PL Begin
                //Gross Profit
                var gross_profit = Number(jQuery(target_el+" :input[name='total_sales[]']").val()) - Number(jQuery(target_el+" :input[name='raw_materials[]']").val()) - Number(jQuery(target_el+" :input[name='cost_others[]']").val());
                
                jQuery(target_el+" :input[name='gross_proft[]']").val(gross_profit);
                
                //Net operating profit
                var net_op_profit = gross_profit - Number(jQuery(target_el+" :input[name='other_admin_exp[]']").val()) - Number(jQuery(target_el+" :input[name='depreciation[]']").val());
                
                jQuery(target_el+" :input[name='net_operating_profit[]']").val(net_op_profit);
                
                //Net Profit
                
                net_profit = net_op_profit + Number(jQuery(target_el+" :input[name='non_ops_income[]']").val()) - Number(jQuery(target_el+" :input[name='finance_cost[]']").val());
    
               // cm_pl_net_profit = net_profit;
                                                                                                                       
                jQuery(target_el+" :input[name='net_profit[]']").val(net_profit);

                //PAT
                var pat = net_profit - Number(jQuery(target_el+" :input[name='taxes[]']").val());
                //cm_pl_pat = pat;
                jQuery(target_el+" :input[name='pat[]']").val(pat);
}

function analysis(target_el)
{
                //Tangible Networth Begin
                var analysis_total_eq = Number(jQuery(target_el+" :input[name='capital[]']").val()) + Number(jQuery(target_el+" :input[name='reserve_surplus[]']").val());
                var tang_networth = analysis_total_eq - Number(jQuery(target_el+" :input[name='intangibles[]']").val());

                jQuery(target_el+" :input[name='tan_networth[]']").val(tang_networth);

                //cm_pl_tange_nw = tang_networth;
                //Tangible Networth End 

                //Working Capital Begin
                var analysis_total_ca = Number(jQuery(target_el+" :input[name='inventory[]']").val()) + Number(jQuery(target_el+" :input[name='receivable[]']").val()) + Number(jQuery(target_el+" :input[name='cash[]']").val()) + Number(jQuery(target_el+" :input[name='other_ca[]']").val());

                var analysis_total_cli = Number(jQuery(target_el+" :input[name='st_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='st_loan_others[]']").val()) + Number(jQuery(target_el+" :input[name='creditors[]']").val()) + Number(jQuery(target_el+" :input[name='ct_others[]']").val());

                var working_capital = analysis_total_ca - analysis_total_cli ;
                jQuery(target_el+" :input[name='work_capital[]']").val(working_capital);
                //Working Capital End

                // Total Borrowing Begin
                var total_brrowing = Number(jQuery(target_el+" :input[name='st_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='st_loan_others[]']").val()) + 
                                     Number(jQuery(target_el+" :input[name='lt_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='lt_loan_others[]']").val()) ;
                
                //cm_pl_total_borrowing = total_borrowing;
                jQuery(target_el+" :input[name='tot_borrowing[]']").val(total_brrowing);
                // Total Borrowing End

                // Capital Employed Begin
                
                var capital_employed = tang_networth + total_brrowing + Number(jQuery(target_el+" :input[name='nc_others[]']").val());
                jQuery(target_el+" :input[name='cap_emp[]']").val(capital_employed);
               
                //cm_pl_cap_emp = capital_employed;
                // End
}

function calc_turnover_ratios(target_el){
    var col_period = (Number(jQuery(target_el+" :input[name='receivable[]']").val())/Number(jQuery(target_el+" :input[name='total_sales[]']").val()))*365;
    jQuery(target_el+" :input[name='coll_period_days[]']").val(col_period);
    var ac_pay_days = (Number(jQuery(target_el+" :input[name='creditors[]']").val())/Number(jQuery(target_el+" :input[name='raw_materials[]']").val()))*365;
    jQuery(target_el+" :input[name='acc_payable_days[]']").val(ac_pay_days);
    var sale_inventory =(Number(jQuery(target_el+" :input[name='total_sales[]']").val())/Number(jQuery(target_el+" :input[name='inventory[]']").val()));
    jQuery(target_el+" :input[name='sale_inv_times[]']").val(sale_inventory);
    var sale_nwc = (Number(jQuery(target_el+" :input[name='total_sales[]']").val())/(Number(jQuery(target_el+" :input[name='total_ca[]']").val()) - Number(jQuery(target_el+" :input[name='total_cl[]']").val())));
    jQuery(target_el+" :input[name='sale_nwc_times[]']").val(sale_nwc);
    var assets_sales = (Number(jQuery(target_el+" :input[name='total_assets[]']").val()) - (Number(jQuery(target_el+" :input[name='intangibles[]']").val())))/ Number(jQuery(target_el+" :input[name='total_sales[]']").val())*100;
    jQuery(target_el+" :input[name='assets_sales_pct[]']").val(assets_sales);
    var assets_turnover = Number(jQuery(target_el+" :input[name='total_sales[]']").val()) / Number(jQuery(target_el+" :input[name='fixed_assets[]']").val());
    jQuery(target_el+" :input[name='fixed_assets_ratio[]']").val(assets_turnover);
}
      
function calc_profitability_liquidity_ratios(target_el){

// -------------------------------------

    var cplr_gross_profit = Number(jQuery(target_el+" :input[name='total_sales[]']").val()) - Number(jQuery(target_el+" :input[name='raw_materials[]']").val()) - Number(jQuery(target_el+" :input[name='cost_others[]']").val());
    //Net operating profit
    var cplr_net_op_profit = cplr_gross_profit - Number(jQuery(target_el+" :input[name='other_admin_exp[]']").val()) - Number(jQuery(target_el+" :input[name='depreciation[]']").val());            
    //Net Profit
    var cplr_net_profit = cplr_net_op_profit + Number(jQuery(target_el+" :input[name='non_ops_income[]']").val()) - Number(jQuery(target_el+" :input[name='finance_cost[]']").val());  

    var cplr_pat = cplr_net_profit - Number(jQuery(target_el+" :input[name='taxes[]']").val());

    var cplr_total_ca = Number(jQuery(target_el+" :input[name='inventory[]']").val()) + Number(jQuery(target_el+" :input[name='receivable[]']").val()) + Number(jQuery(target_el+" :input[name='cash[]']").val()) + Number(jQuery(target_el+" :input[name='other_ca[]']").val());

    var cplr_total_cli = Number(jQuery(target_el+" :input[name='st_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='st_loan_others[]']").val()) + Number(jQuery(target_el+" :input[name='creditors[]']").val()) + Number(jQuery(target_el+" :input[name='ct_others[]']").val());  

    var cplr_total_eq = Number(jQuery(target_el+" :input[name='capital[]']").val()) + Number(jQuery(target_el+" :input[name='reserve_surplus[]']").val());  
    var cplr_tang_networth = cplr_total_eq - Number(jQuery(target_el+" :input[name='intangibles[]']").val());

    var cplr_total_brrowing = Number(jQuery(target_el+" :input[name='st_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='st_loan_others[]']").val()) + 
                                     Number(jQuery(target_el+" :input[name='lt_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='lt_loan_others[]']").val()) ;

    var cplr_capital_employed = cplr_tang_networth + cplr_total_brrowing + Number(jQuery(target_el+" :input[name='nc_others[]']").val());

// --------------------------------------    

    var gprofit_margin = (Number(jQuery(target_el+" :input[name='gross_proft[]']").val())/Number(jQuery(target_el+" :input[name='total_sales[]']").val()))*100;
    jQuery(target_el+" :input[name='gr_profit_margin[]']").val(gprofit_margin);

    var oprofit_margin = (cplr_net_op_profit/Number(jQuery(target_el+" :input[name='total_sales[]']").val()))*100;
    jQuery(target_el+" :input[name='op_profit_margin[]']").val(oprofit_margin);

    var nprofit_margin = (cplr_pat/Number(jQuery(target_el+" :input[name='total_sales[]']").val()))*100;
    jQuery(target_el+" :input[name='net_profit_margin[]']").val(nprofit_margin);
    
    var roa = (cplr_pat/(Number(jQuery(target_el+" :input[name='total_assets[]']").val()) - (Number(jQuery(target_el+" :input[name='intangibles[]']").val()))))*100;
    jQuery(target_el+" :input[name='return_assets[]']").val(roa);
    
    var roe = (cplr_pat/cplr_tang_networth)*100;
    console.log(roe);
    console.log(jQuery(target_el+" :input[name='ret_networth[]']"));
    jQuery(target_el+" :input[name='ret_networth[]']").val(roe);
    
    var roc = (cplr_pat+Number(jQuery(target_el+" :input[name='finance_cost[]']").val())/cplr_capital_employed)*100;
    jQuery(target_el+" :input[name='return_capital[]']").val(roc); 
    
    var quick_ratio = (cplr_total_ca - Number(jQuery(target_el+" :input[name='inventory[]']").val()))/cplr_total_cli;
    jQuery(target_el+" :input[name='quick_ratio[]']").val(quick_ratio); 
    
    var current_ratio = (cplr_total_ca/cplr_total_cli);
    jQuery(target_el+" :input[name='current_ratio[]']").val(current_ratio);     
}

function gearing_solvency_ratios(target_el)
      {
// -------------------------------------
    var gsr_gross_profit = Number(jQuery(target_el+" :input[name='total_sales[]']").val()) - Number(jQuery(target_el+" :input[name='raw_materials[]']").val()) - Number(jQuery(target_el+" :input[name='cost_others[]']").val());
    //Net operating profit
    var gsr_net_op_profit = gsr_gross_profit - Number(jQuery(target_el+" :input[name='other_admin_exp[]']").val()) - Number(jQuery(target_el+" :input[name='depreciation[]']").val());            
    //Net Profit
    var gsr_net_profit = gsr_net_op_profit + Number(jQuery(target_el+" :input[name='non_ops_income[]']").val()) - Number(jQuery(target_el+" :input[name='finance_cost[]']").val());  

    var gsr_total_eq = Number(jQuery(target_el+" :input[name='capital[]']").val()) + Number(jQuery(target_el+" :input[name='reserve_surplus[]']").val());  
    var gsr_tang_networth = gsr_total_eq - Number(jQuery(target_el+" :input[name='intangibles[]']").val());

    var gsr_total_cli = Number(jQuery(target_el+" :input[name='st_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='st_loan_others[]']").val()) + Number(jQuery(target_el+" :input[name='creditors[]']").val()) + Number(jQuery(target_el+" :input[name='ct_others[]']").val());  

    var gsr_total_nli = Number(jQuery(target_el+" :input[name='lt_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='lt_loan_others[]']").val()) + Number(jQuery(target_el+" :input[name='nc_others[]']").val());

    var gsr_total_brrowing = Number(jQuery(target_el+" :input[name='st_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='st_loan_others[]']").val()) + 
                                     Number(jQuery(target_el+" :input[name='lt_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='lt_loan_others[]']").val()) ;
// --------------------------------------         

          var lib_nw =((gsr_total_cli + gsr_total_nli)/gsr_tang_networth)*100;
          jQuery(target_el+" :input[name='liab_networth[]']").val(lib_nw);
          var borrowing_nw = (gsr_total_brrowing/gsr_tang_networth);
          jQuery(target_el+" :input[name='borrow_networth_ratio[]']").val(borrowing_nw);
//          var ebit_ta = (cm_pl_net_profit - Number(jQuery(target_el+" :input[name='finance_cost[]']").val()))/(Number(jQuery(target_el+" :input[name='total_assets[]']").val()) - (Number(jQuery(target_el+" :input[name='intangibles[]']").val())));
//          jQuery(target_el+" :input[name='ebit_assets_z[]']").val(ebit_ta);
          var debt_equity_ratio = gsr_total_nli/(gsr_total_eq-(Number(jQuery(target_el+" :input[name='intangibles[]']").val())));
          jQuery(target_el+" :input[name='debt_equity_ratio[]']").val(debt_equity_ratio);          
          
          var icr = (gsr_net_profit +(Number(jQuery(target_el+" :input[name='finance_cost[]']").val())))/Number(jQuery(target_el+" :input[name='finance_cost[]']").val());
          jQuery(target_el+" :input[name='int_coverage_ratio[]']").val(icr);          
          
          var dscr = (gsr_net_profit +(Number(jQuery(target_el+" :input[name='finance_cost[]']").val()))+(Number(jQuery(target_el+" :input[name='depreciation[]']").val())))/((((Number(jQuery(target_el+" :input[name='lt_loan_bank[]']").val())))/3)+(Number(jQuery(target_el+" :input[name='finance_cost[]']").val())));
          jQuery(target_el+" :input[name='dscr[]']").val(dscr);
      }

function calc_z_score(target_el)
{
//-------------------------------
        var czs_total_ca = Number(jQuery(target_el+" :input[name='inventory[]']").val()) + Number(jQuery(target_el+" :input[name='receivable[]']").val()) + Number(jQuery(target_el+" :input[name='cash[]']").val()) + Number(jQuery(target_el+" :input[name='other_ca[]']").val());
                
        var czs_total_cli = Number(jQuery(target_el+" :input[name='st_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='st_loan_others[]']").val()) + Number(jQuery(target_el+" :input[name='creditors[]']").val()) + Number(jQuery(target_el+" :input[name='ct_others[]']").val());

        var czs_total_nca = Number(jQuery(target_el+" :input[name='fixed_assets[]']").val()) + Number(jQuery(target_el+" :input[name='investments[]']").val()) + Number(jQuery(target_el+" :input[name='other_assets[]']").val()) + Number(jQuery(target_el+" :input[name='intangibles[]']").val());

        var czs_working_capital = czs_total_ca - czs_total_cli ;

        var czs_intangibles = Number(jQuery(target_el+" :input[name='intangibles[]']").val());
        
        var czs_reverse_surplus = Number(jQuery(target_el+" :input[name='reserve_surplus[]']").val());

    //czs_ebit_by_ta
    var czs_gross_profit = Number(jQuery(target_el+" :input[name='total_sales[]']").val()) - Number(jQuery(target_el+" :input[name='raw_materials[]']").val()) - Number(jQuery(target_el+" :input[name='cost_others[]']").val());
    //Net operating profit(c35)
    var czs_net_op_profit = czs_gross_profit - Number(jQuery(target_el+" :input[name='other_admin_exp[]']").val()) - Number(jQuery(target_el+" :input[name='depreciation[]']").val());  
    var  czs_net_profit = czs_net_op_profit + Number(jQuery(target_el+" :input[name='non_ops_income[]']").val()) - Number(jQuery(target_el+" :input[name='finance_cost[]']").val());
    var czs_ebit = czs_net_profit - Number(jQuery(target_el+" :input[name='finance_cost[]']").val());

    //networth_liab_z
     var czs_total_eq = Number(jQuery(target_el+" :input[name='capital[]']").val()) + Number(jQuery(target_el+" :input[name='reserve_surplus[]']").val());
    var czs_tang_networth = czs_total_eq - Number(jQuery(target_el+" :input[name='intangibles[]']").val());
    var czv_total_cli = Number(jQuery(target_el+" :input[name='st_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='st_loan_others[]']").val()) + Number(jQuery(target_el+" :input[name='creditors[]']").val()) + Number(jQuery(target_el+" :input[name='ct_others[]']").val());
    var czv_total_ncli = Number(jQuery(target_el+" :input[name='lt_loan_bank[]']").val()) + Number(jQuery(target_el+" :input[name='lt_loan_others[]']").val()) + Number(jQuery(target_el+" :input[name='nc_others[]']").val());


//-------------------------------
        var czs_wc_by_ta  = (czs_working_capital) / (czs_total_ca + czs_total_nca - czs_intangibles);
        jQuery(target_el+" :input[name='work_capital_assets_z[]']").val(czs_wc_by_ta);

        var czs_re_by_ta = (czs_reverse_surplus) / ( czs_total_ca + czs_total_nca - czs_intangibles );
        jQuery(target_el+" :input[name='r_earn_assets_z[]']").val(czs_re_by_ta);

        var czs_ebit_by_ta = (czs_ebit) / ( czs_total_ca + czs_total_nca - czs_intangibles ); 
        jQuery(target_el+" :input[name='ebit_assets_z[]']").val(czs_ebit_by_ta);

        var czs_nw_by_tl = (czs_tang_networth) / (czv_total_cli + czv_total_ncli);
        jQuery(target_el+" :input[name='networth_liab_z[]']").val(czs_nw_by_tl);

        var czs_s_by_ta  = Number(jQuery(target_el+" :input[name='total_sales[]']").val())  / ( czs_total_ca + czs_total_nca - czs_intangibles ); 
        jQuery(target_el+" :input[name='sales_assets_z[]']").val(czs_s_by_ta);

        var csv_rev_score_z = (0.717 * czs_wc_by_ta + 0.847 * czs_re_by_ta + 3.107 * czs_ebit_by_ta + 0.42 * czs_nw_by_tl + 0.998 * czs_s_by_ta) ;
        jQuery(target_el+" :input[name='rev_score_z[]']").val(csv_rev_score_z);

        var csv_zeta_score = (6.56 * czs_wc_by_ta + 3.26 * czs_re_by_ta + 6.72 * czs_ebit_by_ta + 1.05 * czs_nw_by_tl);
        jQuery(target_el+" :input[name='zeta_score_z[]']").val(csv_zeta_score);

}

function cash_flow_statement_from_operating_activites(target_el)
{
    //NET PROFIT BEFORE TAX AND EXTRAORDINARY ITEMS
    // var oa_gross_profit = Number(jQuery(target_el+" :input[name='total_sales[]']").val()) - Number(jQuery(target_el+" :input[name='raw_materials[]']").val()) - Number(jQuery(target_el+" :input[name='cost_others[]']").val());
    // var oa_net_op_profit = oa_gross_profit - Number(jQuery(target_el+" :input[name='other_admin_exp[]']").val()) - Number(jQuery(target_el+" :input[name='depreciation[]']").val());  
    // var oa_net_profit = oa_net_op_profit + Number(jQuery(target_el+" :input[name='non_ops_income[]']").val()) - Number(jQuery(target_el+" :input[name='finance_cost[]']").val());
    // var oa_ebit = oa_net_profit - Number(jQuery(target_el+" :input[name='finance_cost[]']").val());
    // jQuery(target_el+" :input[name='pbtx[]']").val(oa_ebit);

    var oa_ebit =  Number(jQuery(target_el+" :input[name='net_profit[]']").val());
    jQuery(target_el+" :input[name='pbtx[]']").val(oa_ebit);

    console.log(oa_ebit);

    var oa_pyear_cf = Number(jQuery(target_el+" :input[name='prior_year_adjustments[]']").val());
    jQuery(target_el+" :input[name='pyear_adjustments_cf[]']").val(oa_pyear_cf);

    var oa_depreciation = Number(jQuery(target_el+" :input[name='depreciation[]']").val());
    jQuery(target_el+" :input[name='dep_cf[]']").val(oa_depreciation);

    var oa_non_ops_income = - (Number(jQuery(target_el+" :input[name='non_ops_income[]']").val()));
    jQuery(target_el+" :input[name='non_op_income_cf[]']").val(oa_non_ops_income);

    var oa_non_operating_exp =  - (Number(jQuery(target_el+" :input[name='non_operating_exp[]']").val()));
    jQuery(target_el+" :input[name='non_op_exp_cf[]']").val(oa_non_operating_exp);

    var oa_finance_cost = Number(jQuery(target_el+" :input[name='finance_cost[]']").val());
    jQuery(target_el+" :input[name='int_exp_cf[]']").val(oa_finance_cost);

    var oa_subtotal_cf = oa_pyear_cf + oa_depreciation + oa_non_ops_income + oa_non_operating_exp + oa_finance_cost ;
    jQuery(target_el+" :input[name='subtotal_cf[]']").val(oa_subtotal_cf);

    var wc_op_cp_cf = oa_subtotal_cf + oa_ebit ;
    jQuery(target_el+" :input[name='op_cp_cf[]']").val(wc_op_cp_cf);
}

function operating_profit_before_working_capital(target_el){
    var acc_rec = Number(jQuery("#form_0 :input[name='receivable[]']").val()) - Number(jQuery("#form_1 :input[name='receivable[]']").val());
    jQuery("#form_1 :input[name='acc_receivables_cf[]']").val(acc_rec);

    var acc_rec1 = Number(jQuery("#form_1 :input[name='receivable[]']").val()) - Number(jQuery("#form_2 :input[name='receivable[]']").val());
    jQuery("#form_2 :input[name='acc_receivables_cf[]']").val(acc_rec1);

    var other_rec = Number(jQuery("#form_0 :input[name='other_ca[]']").val()) - Number(jQuery("#form_1 :input[name='other_ca[]']").val());
    jQuery("#form_1 :input[name='other_receivables_cf[]']").val(other_rec);

    var other_rec1 = Number(jQuery("#form_1 :input[name='other_ca[]']").val()) - Number(jQuery("#form_2 :input[name='other_ca[]']").val());
    jQuery("#form_2 :input[name='other_receivables_cf[]']").val(other_rec1);

    var inventory = Number(jQuery("#form_0 :input[name='inventory[]']").val()) - Number(jQuery("#form_1 :input[name='inventory[]']").val());
    jQuery("#form_1 :input[name='inventories_cf[]']").val(inventory);

    var inventory1 = Number(jQuery("#form_1 :input[name='inventory[]']").val()) - Number(jQuery("#form_2 :input[name='inventory[]']").val());
    jQuery("#form_2 :input[name='inventories_cf[]']").val(inventory1);

    var tax_payable =  Number(jQuery("#form_1 :input[name='creditors[]']").val()) -  Number(jQuery("#form_0 :input[name='creditors[]']").val());
    jQuery("#form_1 :input[name='trade_payables_cf[]']").val(tax_payable);

    var tax_payable1 = Number(jQuery("#form_2 :input[name='creditors[]']").val()) - Number(jQuery("#form_1 :input[name='creditors[]']").val());
    jQuery("#form_2 :input[name='trade_payables_cf[]']").val(tax_payable1);


    var other_payable= Number(jQuery("#form_1 :input[name='ct_others[]']").val()) - Number(jQuery("#form_0 :input[name='ct_others[]']").val()) ;
    jQuery("#form_1 :input[name='oth_payables_cf[]']").val(other_payable);

    var other_payable1 =  Number(jQuery("#form_2 :input[name='ct_others[]']").val()) - Number(jQuery("#form_1 :input[name='ct_others[]']").val()); 
    jQuery("#form_2 :input[name='oth_payables_cf[]']").val(other_payable1);

    //console.log(Number(jQuery("#form_1 :input[name='op_cp_cf[]']").val()));
    var netcash_from_oa = Number(jQuery("#form_1 :input[name='op_cp_cf[]']").val()) + acc_rec + other_rec + inventory + tax_payable + other_payable;
    jQuery("#form_1 :input[name='net_op_cash_cf[]']").val(netcash_from_oa);

    // console.log(Number(jQuery("#form_2 :input[name='op_cp_cf[]']").val()));
    var netcash_from_oa1 = Number(jQuery("#form_2 :input[name='op_cp_cf[]']").val()) + acc_rec1 + other_rec1 + inventory1 + tax_payable1 + other_payable1;
     jQuery("#form_2 :input[name='net_op_cash_cf[]']").val(netcash_from_oa1);

}
  </script>
    <div class="container">
    <form name="bs_pl_form" method="POST" action="">
    <div id="form_0" class="container form_container col-md-4">
        <div class="balance_sheet_form col-md-12">
            <h1>Balance Sheet for <span class="bs_pl_sheet_date"></span></h1>
            <div class="total_assets_section">
                <div class="current_assets form-group">
                    <h2>Curent Assets</h2>
                    <div class="input-group">
                        <span class="input-group-addon">Inventory Rs.</span>
                        <input type="number" name="inventory[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Recievable Rs.</span>
                        <input type="number" name ="receivable[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Cash Rs.</span>
                        <input type="number" name ="cash[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Other CA Rs.</span>
                        <input type="number" name ="other_ca[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Total CA Rs.</span>
                        <input type="number" readonly name ="total_ca[]" class="form-control calculated">
                    </div>
                </div>
                <div class="non_current_assets form-group">
                    <h2>Non Current Assets</h2>
                    <div class="input-group">
                            <span class="input-group-addon">Fixed Assets Rs.</span>
                            <input type="number" name="fixed_assets[]" class="form-control">
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Investments Rs.</span>
                            <input type="number" name ="investments[]" class="form-control">
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Other Assets Rs.</span>
                            <input type="number" name ="other_assets[]" class="form-control">
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Intangibles Rs.</span>
                            <input type="number" name ="intangibles[]" class="form-control">
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Total NCA Rs.</span>
                            <input type="number" readonly name ="total_nca[]" class="form-control readonly">
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Total Assets Rs.</span>
                    <input type="number" readonly name="total_assets[]" value="0" class="calculated form-control">
                </div>
            </div>
            <div class="total_liabilities_section">
                <div class="current_liabilities form-group">
                    <h2>Current Liabilities</h2>
                    <div class="input-group">
                        <span class="input-group-addon">ST Loan From Bank Rs.</span>
                        <input type="number" name="st_loan_bank[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">ST Loan From Others Rs.</span>
                        <input type="number" name="st_loan_others[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Creditors Rs.</span>
                        <input type="number" name="creditors[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Others Rs.</span>
                        <input type="number" name="ct_others[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Total Current Liabilities Rs.</span>
                        <input type="number" readonly name="total_cl[]" value="0" class="calculated form-control">
                    </div>
                </div>
                <div class="non_current_liabilities form-group">
                    <h2>Non-Current Liabilities</h2>
                    <div class="input-group">
                        <span class="input-group-addon">LT Loan From Bank Rs.</span>
                        <input type="number" name="lt_loan_bank[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">LT Loan From Others Rs.</span>
                        <input type="number" name="lt_loan_others[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Others Rs.</span>
                        <input type="number" name="nc_others[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Total Non Current Liabilities Rs.</span>
                        <input type="number" readonly name="total_ncl[]" value="0" class="calculated form-control">
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Total Liabilities Rs.</span>
                    <input type="number" readonly name="total_liabilities[]" class="form-control calculated">
                </div>
            </div>
            <div class="total_equity_section">
                <div class="equity form-group">
                    <h2>Equity</h2>
                    <div class="input-group">
                    <span class="input-group-addon">Capital Rs.</span>
                    <input type="number" name="capital[]" class="form-control">
                </div>
                    <div class="input-group">
                    <span class="input-group-addon">Reserve & Suruplus Rs.</span>
                    <input type="number" name="reserve_surplus[]" class="form-control">
                </div>                
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Total Equity Rs.</span>
                    <input type="number" readonly name="total_equity[]" value="0" class="form-control calculated">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="profit_loss_sheet_form col-md-12">
            <h1>P/L for <span class="bs_pl_sheet_date"></span></h1>
                <div class="field-group">
                    <div class="input-group">
                        <span class="input-group-addon">Total Sales Rs.</span>
                        <input type="number" class="form-control" name="total_sales[]">
                    </div>
                    <div class="form-group cost_sales">
                        <h2>Cost of Sales</h2>
                        <div class="input-group">
                            <span class="input-group-addon">Raw Material Rs.</span>
                            <input type="number" class="form-control" name="raw_materials[]">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">Others Rs.</span>
                            <input type="number" class="form-control" name="cost_others[]">
                        </div>
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Gross Profit Rs.</span>
                            <input type="number" readonly name="gross_proft[]" class="form-control calculated">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Other Admin Exp. Rs.</span>
                        <input type="number" name="other_admin_exp[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Depreciation Rs.</span>
                        <input type="number" name="depreciation[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Net Operating Profit Rs.</span>
                        <input type="number" readonly name="net_operating_profit[]" class="form-control calculated">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Non Ops Income Rs.</span>
                        <input type="number" name="non_ops_income[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Finance Cost Rs.</span>
                        <input type="number" name="finance_cost[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Net Profit Rs.</span>
                        <input type="number" readonly name="net_profit[]" class="form-control calculated">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Taxes Rs.</span>
                        <input type="number" name="taxes[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">PAT Rs.</span>
                        <input type="number" readonly name="pat[]" class="calculated form-control">
                    </div>
                </div>
        </div>
        <div class="clearfix"></div>
        <div class="analysis_section_form col-md-12">
            <h1> Analysis</h1>
            <div class="field-group">
                <div class="input-group">
                    <span class="input-group-addon">Tangible Networth Rs.</span>
                    <input type="text" readonly name="tan_networth[]" class="calculated form-control">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Working Capital Rs.</span>
                    <input type="text" readonly name="work_capital[]" class="calculated form-control">
                </div>                
                <div class="input-group">
                    <span class="input-group-addon">Total Borrowing Rs.</span>
                    <input type="text" readonly name="tot_borrowing[]" class="calculated form-control">
                </div>               
                <div class="input-group">
                    <span class="input-group-addon">Capital Employed Rs.</span>
                    <input type="text" readonly name="cap_emp[]" class="calculated form-control">
                </div>                
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="adjustments_form_0 col-md-12">
            <h1>Adjustments</h1>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">Prior Year Adjustments Rs.</span>
                    <input type="number" name="prior_year_adjustments[]" class="form-control">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Non Operating Expenses Rs.</span>
                    <input type="number" name="non_operating_exp[]" class="form-control">
                </div>
            </div>                
        </div>
        <div class="clearfix"></div>
        <div class="ratios_form col-md-12">
            <h1>Ratios</h1>
            <div class="field-group turnover_ratios">
                <h2>Turnover Ratios:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Collection Period (Days)</span>
                    <input type="number" readonly class="calculated form-control" name="coll_period_days[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Account Payable (Days)</span>
                    <input type="number" readonly class="calculated form-control" name="acc_payable_days[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Sales to Inventory (Times)</span>
                    <input type="number" readonly class="calculated form-control" name="sale_inv_times[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Sales to Net Working Capital (Times)</span>
                    <input type="number" readonly class="calculated form-control" name="sale_nwc_times[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Assets to Sales (%)</span>
                    <input type="number" readonly class="calculated form-control" name="assets_sales_pct[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Fixed Assets Turnover Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="fixed_assets_ratio[]">
                </div>                
            </div>
           <div class="field-group prof_ratios">
                <h2>Profitability Ratios:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Gross Profit Margin (%)</span>
                    <input type="number" readonly class="calculated form-control" name="gr_profit_margin[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Operating Profit Margin (%)</span>
                    <input type="number" readonly class="calculated form-control" name="op_profit_margin[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Net Profit Margin (%)</span>
                    <input type="number" readonly class="calculated form-control" name="net_profit_margin[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Return on Assets (%))</span>
                    <input type="number" readonly class="calculated form-control" name="return_assets[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Return on Networth (Return on Equity) (%)</span>
                    <input type="number" readonly class="calculated form-control" name="ret_networth[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Return on Capital Employed (%)</span>
                    <input type="number" readonly class="calculated form-control" name="return_capital[]">
                </div>                
            </div>
           <div class="field-group liquid_ratios">
                <h2>Liquidity Ratios:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Quick Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="quick_ratio[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Current Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="current_ratio[]">
                </div>
            </div>
            <div class="field-group liquid_ratios">
                <h2>Gearing & Solvency Ratios:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Total Liabilities to Tangible Net Worth (%)</span>
                    <input type="number" readonly class="calculated form-control" name="liab_networth[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Total Borrowing to Tangible Net Worth Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="borrow_networth_ratio[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Debt Equity Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="debt_equity_ratio[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Interest Coverage Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="int_coverage_ratio[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">DSCR</span>
                    <input type="number" readonly class="calculated form-control" name="dscr[]">
                </div>
            </div>
            <div class="field-group liquid_ratios">
                <h2>Z-Score:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Working Capital/Total Assets</span>
                    <input type="number" readonly class="calculated form-control" name="work_capital_assets_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Retained Earning/Total Assets</span>
                    <input type="number" readonly class="calculated form-control" name="r_earn_assets_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">EBIT/Total Assets</span>
                    <input type="number" readonly class="calculated form-control" name="ebit_assets_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Net worth/Total Liabilities</span>
                    <input type="number" readonly class="calculated form-control" name="networth_liab_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Sales/Total Assets</span>
                    <input type="number" readonly class="calculated form-control" name="sales_assets_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Revised Z-Score</span>
                    <input type="number" readonly class="calculated form-control" name="rev_score_z[]">
                </div>                
                <div class="input-group">
                    <span class="input-group-addon">Zeta Score</span>
                    <input type="number" readonly class="calculated form-control" name="zeta_score_z[]">
                </div>                                
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="cashflow_form col-md-12">
            <h1>Cash flow Statement from Operating Activities</h1>
            <div class="field-group cashflow">
                <label>Net Profit Before Tax and Ex. Items</label>
                <div class="input-group">
                    <span class="input-group-addon">Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="pbtx[]">
                </div>
                <label>Adjustments For:</label>
                <div class="input-group">
                    <span class="input-group-addon">Prior Year Adjustments Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="pyear_adjustments_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Depreciation Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="dep_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Non-Operating Income Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="non_op_income_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Non-Operating Expenses Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="non_op_exp_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Interest Expense Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="int_exp_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><strong>Subtotal Rs.</strong></span>
                    <input type="number" readonly class="form-control readonly" name="subtotal_cf[]">
                </div>
                <label>Operating Profit Before Working Capital Changes</label>
                <div class="input-group">
                    <span class="input-group-addon">Rs. </span>
                    <input type="number" readonly class="form-control readonly" name="op_cp_cf[]">
                </div>
                <label>Adjustments For:</label>
                <div class="input-group">
                    <span class="input-group-addon">Accounts Receivables</span>
                    <input type="number" readonly class="form-control readonly" name="acc_receivables_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Other Receivables (Inc. Taxes)</span>
                    <input type="number" readonly class="form-control readonly" name="other_receivables_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Inventories</span>
                    <input type="number" readonly class="form-control readonly" name="inventories_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Trade Payables</span>
                    <input type="number" readonly class="form-control readonly" name="trade_payables_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Other Paybales</span>
                    <input type="number" readonly class="form-control readonly" name="oth_payables_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Extraordinary Items</span>
                    <input type="number" readonly class="form-control readonly" name="ex_items_cf[]">
                </div>
                <label>Net Cash From Operating Activities</label>
                <div class="input-group">
                    <span class="input-group-addon">Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="net_op_cash_cf[]">
                </div>
            </div>
        </div>
    </div>
    <div id="form_1" class="container form_container col-md-4">
        <div class="balance_sheet_form col-md-12">
            <h1>Balance Sheet for <span class="bs_pl_sheet_date"></span></h1>
            <div class="total_assets_section">
                <div class="current_assets form-group">
                    <h2>Curent Assets</h2>
                    <div class="input-group">
                        <span class="input-group-addon">Inventory Rs.</span>
                        <input type="number" name="inventory[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Recievable Rs.</span>
                        <input type="number" name ="receivable[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Cash Rs.</span>
                        <input type="number" name ="cash[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Other CA Rs.</span>
                        <input type="number" name ="other_ca[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Total CA Rs.</span>
                        <input type="number" readonly name ="total_ca[]" class="form-control calculated">
                    </div>
                </div>
                <div class="non_current_assets form-group">
                    <h2>Non Current Assets</h2>
                    <div class="input-group">
                            <span class="input-group-addon">Fixed Assets Rs.</span>
                            <input type="number" name="fixed_assets[]" class="form-control">
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Investments Rs.</span>
                            <input type="number" name ="investments[]" class="form-control">
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Other Assets Rs.</span>
                            <input type="number" name ="other_assets[]" class="form-control">
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Intangibles Rs.</span>
                            <input type="number" name ="intangibles[]" class="form-control">
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Total NCA Rs.</span>
                            <input type="number" readonly name ="total_nca[]" class="form-control readonly">
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Total Assets Rs.</span>
                    <input type="number" readonly name="total_assets[]" value="0" class="calculated form-control">
                </div>
            </div>
            <div class="total_liabilities_section">
                <div class="current_liabilities form-group">
                    <h2>Current Liabilities</h2>
                    <div class="input-group">
                        <span class="input-group-addon">ST Loan From Bank Rs.</span>
                        <input type="number" name="st_loan_bank[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">ST Loan From Others Rs.</span>
                        <input type="number" name="st_loan_others[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Creditors Rs.</span>
                        <input type="number" name="creditors[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Others Rs.</span>
                        <input type="number" name="ct_others[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Total Current Liabilities Rs.</span>
                        <input type="number" readonly name="total_cl[]" value="0" class="calculated form-control">
                    </div>
                </div>
                <div class="non_current_liabilities form-group">
                    <h2>Non-Current Liabilities</h2>
                    <div class="input-group">
                        <span class="input-group-addon">LT Loan From Bank Rs.</span>
                        <input type="number" name="lt_loan_bank[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">LT Loan From Others Rs.</span>
                        <input type="number" name="lt_loan_others[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Others Rs.</span>
                        <input type="number" name="nc_others[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Total Non Current Liabilities Rs.</span>
                        <input type="number" readonly name="total_ncl[]" value="0" class="calculated form-control">
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Total Liabilities Rs.</span>
                    <input type="number" readonly name="total_liabilities[]" class="form-control calculated">
                </div>
            </div>
            <div class="total_equity_section">
                <div class="equity form-group">
                    <h2>Equity</h2>
                    <div class="input-group">
                    <span class="input-group-addon">Capital Rs.</span>
                    <input type="number" name="capital[]" class="form-control">
                </div>
                    <div class="input-group">
                    <span class="input-group-addon">Reserve & Suruplus Rs.</span>
                    <input type="number" name="reserve_surplus[]" class="form-control">
                </div>                
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Total Equity Rs.</span>
                    <input type="number" readonly name="total_equity[]" value="0" class="form-control calculated">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="profit_loss_sheet_form col-md-12">
            <h1>P/L for <span class="bs_pl_sheet_date"></span></h1>
                <div class="field-group">
                    <div class="input-group">
                        <span class="input-group-addon">Total Sales Rs.</span>
                        <input type="number" class="form-control" name="total_sales[]">
                    </div>
                    <div class="form-group cost_sales">
                        <h2>Cost of Sales</h2>
                        <div class="input-group">
                            <span class="input-group-addon">Raw Material Rs.</span>
                            <input type="number" class="form-control" name="raw_materials[]">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">Others Rs.</span>
                            <input type="number" class="form-control" name="cost_others[]">
                        </div>
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Gross Profit Rs.</span>
                            <input type="number" readonly name="gross_proft[]" class="form-control calculated">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Other Admin Exp. Rs.</span>
                        <input type="number" name="other_admin_exp[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Depreciation Rs.</span>
                        <input type="number" name="depreciation[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Net Operating Profit Rs.</span>
                        <input type="number" readonly name="net_operating_profit[]" class="form-control calculated">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Non Ops Income Rs.</span>
                        <input type="number" name="non_ops_income[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Finance Cost Rs.</span>
                        <input type="number" name="finance_cost[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Net Profit Rs.</span>
                        <input type="number" readonly name="net_profit[]" class="form-control calculated">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Taxes Rs.</span>
                        <input type="number" name="taxes[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">PAT Rs.</span>
                        <input type="number" readonly name="pat[]" class="calculated form-control">
                    </div>
                </div>
        </div>
        <div class="clearfix"></div>
        <div class="analysis_section_form col-md-12">
            <h1> Analysis</h1>
            <div class="field-group">
                <div class="input-group">
                    <span class="input-group-addon">Tangible Networth Rs.</span>
                    <input type="text" readonly name="tan_networth[]" class="calculated form-control">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Working Capital Rs.</span>
                    <input type="text" readonly name="work_capital[]" class="calculated form-control">
                </div>                
                <div class="input-group">
                    <span class="input-group-addon">Total Borrowing Rs.</span>
                    <input type="text" readonly name="tot_borrowing[]" class="calculated form-control">
                </div>               
                <div class="input-group">
                    <span class="input-group-addon">Capital Employed Rs.</span>
                    <input type="text" readonly name="cap_emp[]" class="calculated form-control">
                </div>                
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="adjustments_form_0 col-md-12">
            <h1>Adjustments</h1>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">Prior Year Adjustments Rs.</span>
                    <input type="number" name="prior_year_adjustments[]" class="form-control">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Non Operating Expenses Rs.</span>
                    <input type="number" name="non_operating_exp[]" class="form-control">
                </div>
            </div>                
        </div>
        <div class="clearfix"></div>
        <div class="ratios_form col-md-12">
            <h1>Ratios</h1>
            <div class="field-group turnover_ratios">
                <h2>Turnover Ratios:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Collection Period (Days)</span>
                    <input type="number" readonly class="calculated form-control" name="coll_period_days[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Account Payable (Days)</span>
                    <input type="number" readonly class="calculated form-control" name="acc_payable_days[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Sales to Inventory (Times)</span>
                    <input type="number" readonly class="calculated form-control" name="sale_inv_times[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Sales to Net Working Capital (Times)</span>
                    <input type="number" readonly class="calculated form-control" name="sale_nwc_times[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Assets to Sales (%)</span>
                    <input type="number" readonly class="calculated form-control" name="assets_sales_pct[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Fixed Assets Turnover Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="fixed_assets_ratio[]">
                </div>                
            </div>
           <div class="field-group prof_ratios">
                <h2>Profitability Ratios:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Gross Profit Margin (%)</span>
                    <input type="number" readonly class="calculated form-control" name="gr_profit_margin[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Operating Profit Margin (%)</span>
                    <input type="number" readonly class="calculated form-control" name="op_profit_margin[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Net Profit Margin (%)</span>
                    <input type="number" readonly class="calculated form-control" name="net_profit_margin[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Return on Assets (%))</span>
                    <input type="number" readonly class="calculated form-control" name="return_assets[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Return on Networth (Return on Equity) (%)</span>
                    <input type="number" readonly class="calculated form-control" name="ret_networth[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Return on Capital Employed (%)</span>
                    <input type="number" readonly class="calculated form-control" name="return_capital[]">
                </div>                
            </div>
           <div class="field-group liquid_ratios">
                <h2>Liquidity Ratios:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Quick Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="quick_ratio[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Current Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="current_ratio[]">
                </div>
            </div>
            <div class="field-group liquid_ratios">
                <h2>Gearing & Solvency Ratios:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Total Liabilities to Tangible Net Worth (%)</span>
                    <input type="number" readonly class="calculated form-control" name="liab_networth[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Total Borrowing to Tangible Net Worth Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="borrow_networth_ratio[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Debt Equity Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="debt_equity_ratio[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Interest Coverage Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="int_coverage_ratio[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">DSCR</span>
                    <input type="number" readonly class="calculated form-control" name="dscr[]">
                </div>
            </div>
            <div class="field-group liquid_ratios">
                <h2>Z-Score:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Working Capital/Total Assets</span>
                    <input type="number" readonly class="calculated form-control" name="work_capital_assets_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Retained Earning/Total Assets</span>
                    <input type="number" readonly class="calculated form-control" name="r_earn_assets_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">EBIT/Total Assets</span>
                    <input type="number" readonly class="calculated form-control" name="ebit_assets_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Net worth/Total Liabilities</span>
                    <input type="number" readonly class="calculated form-control" name="networth_liab_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Sales/Total Assets</span>
                    <input type="number" readonly class="calculated form-control" name="sales_assets_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Revised Z-Score</span>
                    <input type="number" readonly class="calculated form-control" name="rev_score_z[]">
                </div>                
                <div class="input-group">
                    <span class="input-group-addon">Zeta Score</span>
                    <input type="number" readonly class="calculated form-control" name="zeta_score_z[]">
                </div>                                
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="cashflow_form col-md-12">
            <h1>Cash flow Statement from Operating Activities</h1>
            <div class="field-group cashflow">
                <label>Net Profit Before Tax and Ex. Items</label>
                <div class="input-group">
                    <span class="input-group-addon">Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="pbtx[]">
                </div>
                <label>Adjustments For:</label>
                <div class="input-group">
                    <span class="input-group-addon">Prior Year Adjustments Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="pyear_adjustments_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Depreciation Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="dep_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Non-Operating Income Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="non_op_income_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Non-Operating Expenses Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="non_op_exp_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Interest Expense Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="int_exp_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><strong>Subtotal Rs.</strong></span>
                    <input type="number" readonly class="form-control readonly" name="subtotal_cf[]">
                </div>
                <label>Operating Profit Before Working Capital Changes</label>
                <div class="input-group">
                    <span class="input-group-addon">Rs. </span>
                    <input type="number" readonly class="form-control readonly" name="op_cp_cf[]">
                </div>
                <label>Adjustments For:</label>
                <div class="input-group">
                    <span class="input-group-addon">Accounts Receivables</span>
                    <input type="number" readonly class="form-control readonly" name="acc_receivables_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Other Receivables (Inc. Taxes)</span>
                    <input type="number" readonly class="form-control readonly" name="other_receivables_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Inventories</span>
                    <input type="number" readonly class="form-control readonly" name="inventories_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Trade Payables</span>
                    <input type="number" readonly class="form-control readonly" name="trade_payables_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Other Paybales</span>
                    <input type="number" readonly class="form-control readonly" name="oth_payables_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Extraordinary Items</span>
                    <input type="number" readonly class="form-control readonly" name="ex_items_cf[]">
                </div>
                <label>Net Cash From Operating Activities</label>
                <div class="input-group">
                    <span class="input-group-addon">Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="net_op_cash_cf[]">
                </div>
            </div>
        </div>
    </div>
    <div id="form_2" class="container form_container col-md-4">
        <div class="balance_sheet_form col-md-12">
            <h1>Balance Sheet for <span class="bs_pl_sheet_date"></span></h1>
            <div class="total_assets_section">
                <div class="current_assets form-group">
                    <h2>Curent Assets</h2>
                    <div class="input-group">
                        <span class="input-group-addon">Inventory Rs.</span>
                        <input type="number" name="inventory[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Recievable Rs.</span>
                        <input type="number" name ="receivable[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Cash Rs.</span>
                        <input type="number" name ="cash[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Other CA Rs.</span>
                        <input type="number" name ="other_ca[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Total CA Rs.</span>
                        <input type="number" readonly name ="total_ca[]" class="form-control calculated">
                    </div>
                </div>
                <div class="non_current_assets form-group">
                    <h2>Non Current Assets</h2>
                    <div class="input-group">
                            <span class="input-group-addon">Fixed Assets Rs.</span>
                            <input type="number" name="fixed_assets[]" class="form-control">
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Investments Rs.</span>
                            <input type="number" name ="investments[]" class="form-control">
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Other Assets Rs.</span>
                            <input type="number" name ="other_assets[]" class="form-control">
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Intangibles Rs.</span>
                            <input type="number" name ="intangibles[]" class="form-control">
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Total NCA Rs.</span>
                            <input type="number" readonly name ="total_nca[]" class="form-control readonly">
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Total Assets Rs.</span>
                    <input type="number" readonly name="total_assets[]" value="0" class="calculated form-control">
                </div>
            </div>
            <div class="total_liabilities_section">
                <div class="current_liabilities form-group">
                    <h2>Current Liabilities</h2>
                    <div class="input-group">
                        <span class="input-group-addon">ST Loan From Bank Rs.</span>
                        <input type="number" name="st_loan_bank[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">ST Loan From Others Rs.</span>
                        <input type="number" name="st_loan_others[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Creditors Rs.</span>
                        <input type="number" name="creditors[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Others Rs.</span>
                        <input type="number" name="ct_others[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Total Current Liabilities Rs.</span>
                        <input type="number" readonly name="total_cl[]" value="0" class="calculated form-control">
                    </div>
                </div>
                <div class="non_current_liabilities form-group">
                    <h2>Non-Current Liabilities</h2>
                    <div class="input-group">
                        <span class="input-group-addon">LT Loan From Bank Rs.</span>
                        <input type="number" name="lt_loan_bank[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">LT Loan From Others Rs.</span>
                        <input type="number" name="lt_loan_others[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Others Rs.</span>
                        <input type="number" name="nc_others[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Total Non Current Liabilities Rs.</span>
                        <input type="number" readonly name="total_ncl[]" value="0" class="calculated form-control">
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Total Liabilities Rs.</span>
                    <input type="number" readonly name="total_liabilities[]" class="form-control calculated">
                </div>
            </div>
            <div class="total_equity_section">
                <div class="equity form-group">
                    <h2>Equity</h2>
                    <div class="input-group">
                    <span class="input-group-addon">Capital Rs.</span>
                    <input type="number" name="capital[]" class="form-control">
                </div>
                    <div class="input-group">
                    <span class="input-group-addon">Reserve & Suruplus Rs.</span>
                    <input type="number" name="reserve_surplus[]" class="form-control">
                </div>                
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Total Equity Rs.</span>
                    <input type="number" readonly name="total_equity[]" value="0" class="form-control calculated">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="profit_loss_sheet_form col-md-12">
            <h1>P/L for <span class="bs_pl_sheet_date"></span></h1>
                <div class="field-group">
                    <div class="input-group">
                        <span class="input-group-addon">Total Sales Rs.</span>
                        <input type="number" class="form-control" name="total_sales[]">
                    </div>
                    <div class="form-group cost_sales">
                        <h2>Cost of Sales</h2>
                        <div class="input-group">
                            <span class="input-group-addon">Raw Material Rs.</span>
                            <input type="number" class="form-control" name="raw_materials[]">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">Others Rs.</span>
                            <input type="number" class="form-control" name="cost_others[]">
                        </div>
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">Gross Profit Rs.</span>
                            <input type="number" readonly name="gross_proft[]" class="form-control calculated">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Other Admin Exp. Rs.</span>
                        <input type="number" name="other_admin_exp[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Depreciation Rs.</span>
                        <input type="number" name="depreciation[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Net Operating Profit Rs.</span>
                        <input type="number" readonly name="net_operating_profit[]" class="form-control calculated">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Non Ops Income Rs.</span>
                        <input type="number" name="non_ops_income[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Finance Cost Rs.</span>
                        <input type="number" name="finance_cost[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Net Profit Rs.</span>
                        <input type="number" readonly name="net_profit[]" class="form-control calculated">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Taxes Rs.</span>
                        <input type="number" name="taxes[]" class="form-control">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">PAT Rs.</span>
                        <input type="number" readonly name="pat[]" class="calculated form-control">
                    </div>
                </div>
        </div>
        <div class="clearfix"></div>
        <div class="analysis_section_form col-md-12">
            <h1> Analysis</h1>
            <div class="field-group">
                <div class="input-group">
                    <span class="input-group-addon">Tangible Networth Rs.</span>
                    <input type="text" readonly name="tan_networth[]" class="calculated form-control">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Working Capital Rs.</span>
                    <input type="text" readonly name="work_capital[]" class="calculated form-control">
                </div>                
                <div class="input-group">
                    <span class="input-group-addon">Total Borrowing Rs.</span>
                    <input type="text" readonly name="tot_borrowing[]" class="calculated form-control">
                </div>               
                <div class="input-group">
                    <span class="input-group-addon">Capital Employed Rs.</span>
                    <input type="text" readonly name="cap_emp[]" class="calculated form-control">
                </div>                
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="adjustments_form_0 col-md-12">
            <h1>Adjustments</h1>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">Prior Year Adjustments Rs.</span>
                    <input type="number" name="prior_year_adjustments[]" class="form-control">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Non Operating Expenses Rs.</span>
                    <input type="number" name="non_operating_exp[]" class="form-control">
                </div>
            </div>                
        </div>
        <div class="clearfix"></div>
        <div class="ratios_form col-md-12">
            <h1>Ratios</h1>
            <div class="field-group turnover_ratios">
                <h2>Turnover Ratios:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Collection Period (Days)</span>
                    <input type="number" readonly class="calculated form-control" name="coll_period_days[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Account Payable (Days)</span>
                    <input type="number" readonly class="calculated form-control" name="acc_payable_days[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Sales to Inventory (Times)</span>
                    <input type="number" readonly class="calculated form-control" name="sale_inv_times[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Sales to Net Working Capital (Times)</span>
                    <input type="number" readonly class="calculated form-control" name="sale_nwc_times[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Assets to Sales (%)</span>
                    <input type="number" readonly class="calculated form-control" name="assets_sales_pct[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Fixed Assets Turnover Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="fixed_assets_ratio[]">
                </div>                
            </div>
           <div class="field-group prof_ratios">
                <h2>Profitability Ratios:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Gross Profit Margin (%)</span>
                    <input type="number" readonly class="calculated form-control" name="gr_profit_margin[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Operating Profit Margin (%)</span>
                    <input type="number" readonly class="calculated form-control" name="op_profit_margin[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Net Profit Margin (%)</span>
                    <input type="number" readonly class="calculated form-control" name="net_profit_margin[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Return on Assets (%))</span>
                    <input type="number" readonly class="calculated form-control" name="return_assets[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Return on Networth (Return on Equity) (%)</span>
                    <input type="number" readonly class="calculated form-control" name="ret_networth[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Return on Capital Employed (%)</span>
                    <input type="number" readonly class="calculated form-control" name="return_capital[]">
                </div>                
            </div>
           <div class="field-group liquid_ratios">
                <h2>Liquidity Ratios:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Quick Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="quick_ratio[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Current Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="current_ratio[]">
                </div>
            </div>
            <div class="field-group liquid_ratios">
                <h2>Gearing & Solvency Ratios:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Total Liabilities to Tangible Net Worth (%)</span>
                    <input type="number" readonly class="calculated form-control" name="liab_networth[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Total Borrowing to Tangible Net Worth Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="borrow_networth_ratio[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Debt Equity Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="debt_equity_ratio[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Interest Coverage Ratio</span>
                    <input type="number" readonly class="calculated form-control" name="int_coverage_ratio[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">DSCR</span>
                    <input type="number" readonly class="calculated form-control" name="dscr[]">
                </div>
            </div>
            <div class="field-group liquid_ratios">
                <h2>Z-Score:</h2>
                <div class="input-group">
                    <span class="input-group-addon">Working Capital/Total Assets</span>
                    <input type="number" readonly class="calculated form-control" name="work_capital_assets_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Retained Earning/Total Assets</span>
                    <input type="number" readonly class="calculated form-control" name="r_earn_assets_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">EBIT/Total Assets</span>
                    <input type="number" readonly class="calculated form-control" name="ebit_assets_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Net worth/Total Liabilities</span>
                    <input type="number" readonly class="calculated form-control" name="networth_liab_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Sales/Total Assets</span>
                    <input type="number" readonly class="calculated form-control" name="sales_assets_z[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Revised Z-Score</span>
                    <input type="number" readonly class="calculated form-control" name="rev_score_z[]">
                </div>                
                <div class="input-group">
                    <span class="input-group-addon">Zeta Score</span>
                    <input type="number" readonly class="calculated form-control" name="zeta_score_z[]">
                </div>                                
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="cashflow_form col-md-12">
            <h1>Cash flow Statement from Operating Activities</h1>
            <div class="field-group cashflow">
                <label>Net Profit Before Tax and Ex. Items</label>
                <div class="input-group">
                    <span class="input-group-addon">Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="pbtx[]">
                </div>
                <label>Adjustments For:</label>
                <div class="input-group">
                    <span class="input-group-addon">Prior Year Adjustments Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="pyear_adjustments_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Depreciation Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="dep_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Non-Operating Income Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="non_op_income_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Non-Operating Expenses Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="non_op_exp_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Interest Expense Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="int_exp_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><strong>Subtotal Rs.</strong></span>
                    <input type="number" readonly class="form-control readonly" name="subtotal_cf[]">
                </div>
                <label>Operating Profit Before Working Capital Changes</label>
                <div class="input-group">
                    <span class="input-group-addon">Rs. </span>
                    <input type="number" readonly class="form-control readonly" name="op_cp_cf[]">
                </div>
                <label>Adjustments For:</label>
                <div class="input-group">
                    <span class="input-group-addon">Accounts Receivables</span>
                    <input type="number" readonly class="form-control readonly" name="acc_receivables_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Other Receivables (Inc. Taxes)</span>
                    <input type="number" readonly class="form-control readonly" name="other_receivables_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Inventories</span>
                    <input type="number" readonly class="form-control readonly" name="inventories_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Trade Payables</span>
                    <input type="number" readonly class="form-control readonly" name="trade_payables_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Other Paybales</span>
                    <input type="number" readonly class="form-control readonly" name="oth_payables_cf[]">
                </div>
                <div class="input-group">
                    <span class="input-group-addon">Extraordinary Items</span>
                    <input type="number" readonly class="form-control readonly" name="ex_items_cf[]">
                </div>
                <label>Net Cash From Operating Activities</label>
                <div class="input-group">
                    <span class="input-group-addon">Rs.</span>
                    <input type="number" readonly class="form-control readonly" name="net_op_cash_cf[]">
                </div>
            </div>
        </div>
    </div>
        <input type="submit" class="form-control action-btn btn-primary" name="p_l_submit" value="Save"> 
    </form>
    </div>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>